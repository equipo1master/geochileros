function setListeners(){
	document.addEventListener('click', onClickElement, false);
}

function onClickElement(e){
    var focus = e.target || e.srcElement;
    var end = false;

    while(!end){
    	var functionAttr = focus.getAttribute("function");
    	if(functionAttr && functionAttr != ""){
    		if(functionAttr=="generateMap"){
            	initialize();
            }else if(functionAttr=="switchForm"){
            	switchForm(focus.getAttribute("show"));
            }
    		end=true;
		}else if(focus.nodeName == "BODY"){
            end = true;
        }else{
            focus = focus.parentNode;
        }
    }
    // e.stopPropagation();
    // e.preventDefault();
}


function initialize() {
	document.getElementById("genMap").style.visibility="visible";
	document.getElementById("jugar-contenido-interactivo").style.display="inline";
	var lat = chance.latitude({fixed: 6});//-90 + 180 * Math.random();
	var lon = chance.longitude({fixed: 6});//-180 + 360 * Math.random();
	var radio = 50;//1000000;
	var myLatlng = new google.maps.LatLng(lat,lon);
    var service = new google.maps.StreetViewService();
	service.getPanoramaByLocation(myLatlng, radio, handler);
	var map;
	var mapOptions = {
	  zoom: 0,
	  center: new google.maps.LatLng(0,0)
	};
	map = new google.maps.Map(document.getElementById('minimap'),mapOptions);
	function handler(result, status) {
        if (status == google.maps.StreetViewStatus.OK) {
        	var panoramaOptions = {
        		addressControl: false,
			    position: result.location.latLng,
			    pov: {
			      heading: 34,
			      pitch: 10
			    }
			  };
			  var panorama = new google.maps.StreetViewPanorama(document.getElementById('map'), panoramaOptions);
			  map.setStreetView(panorama);
        }
        else {
        	radio+=100000;
        	service.getPanoramaByLocation(myLatlng, radio, handler);
        	/*alert("Ningún street view en un radio de "+radio+"m alrededor de "+myLatlng);
        	return;*/
        }
    }
}

function switchForm(target){
	var original = document.querySelector("div.welcome[status='show']")
	if(original==null){
		document.getElementById("welcomeMessage").setAttribute("status","hide");
	}else{
		original.setAttribute("status","hide");
	}
	document.getElementById(target).setAttribute("status","show");
}

function enableAcceptGroupInvitation()
{
	var elemento = document.getElementById("grupo-aceptar-invitacion");
	var clase = elemento.className;
	clase = clase.replace("disabled","");
	clase = clase.replace("default","success");
	elemento.className = clase.replace("disabled","");
}
